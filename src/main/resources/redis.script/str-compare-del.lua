-- 获取 string key的值，比较String value如果相等则删除，最后操作结果
local cached = redis.call("get", KEYS[1])
-- 如果相等则删除
if (cached == ARGV[1]) then
  redis.call("del", KEYS[1])
  return true
else
  return false
end