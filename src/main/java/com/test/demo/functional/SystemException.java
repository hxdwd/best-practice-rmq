package com.test.demo.functional;

/**
 * 系统异常
 *
 * @author huangxiaodong
 * @date 2021/11/9 19:36
 */
public class SystemException extends RuntimeException {
    public SystemException(String message) {
        super(message);
    }

    public SystemException(String message, Throwable cause) {
        super(message, cause);
    }
}
