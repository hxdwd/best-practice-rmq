package com.test.demo.listener;

import com.test.demo.model.dto.MsgDto;
import com.test.demo.service.IHandleService;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 入口消息监听
 *
 * @author huangxiaodong
 * @date 2021/5/15  15:01
 */
@Slf4j
@Service
@RocketMQMessageListener(topic = "${com.business.mq.entry-topic}",consumerGroup = "${com.business.mq.consumer-group}")
public class EntryListener extends AbstractMsgListener<MsgDto> {

    private IHandleService handleService;

    @Override
    protected void process(MsgDto msg) {
      handleService.handle(msg);
    }

    @Autowired
    public void setHandleService(IHandleService handleService) {
        this.handleService = handleService;
    }
}
