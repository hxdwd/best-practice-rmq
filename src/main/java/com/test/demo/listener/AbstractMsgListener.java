package com.test.demo.listener;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.core.RocketMQListener;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

/**
 * 抽象入口
 *
 * @author huangxiaodong
 * @date 2021/5/15  15:18
 */
@Slf4j
public abstract class AbstractMsgListener<T extends Serializable> implements RocketMQListener<String> {

    @Override
    public void onMessage(String message) {
        log.info("入口消息：{}", message);
        T msg;
        try {
            msg = JSON.parseObject(message, getActualTypeArgument());
        } catch (Exception e) {
            log.error("消息序列化失败", e);
            return;
        }

        // 处理消息
        process(msg);
    }

    private Type getActualTypeArgument() {
        Type type = this.getClass().getGenericSuperclass();
        return ((ParameterizedType) type).getActualTypeArguments()[0];
    }

    /**
     * 消息处理逻辑
     *
     * @param msg 序列化消息
     */
    protected abstract void process(T msg);
}
