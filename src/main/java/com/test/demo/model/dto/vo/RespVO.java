package com.test.demo.model.dto.vo;

import lombok.Builder;
import lombok.Data;
import sun.swing.plaf.synth.DefaultSynthStyle;

/**
 * 返回实体类
 *
 * @author huangxiaodong
 * @date 2021/7/23  11:18
 */
@Data
@Builder
public class RespVO {
    String retCode;
    String retMsg;
}
