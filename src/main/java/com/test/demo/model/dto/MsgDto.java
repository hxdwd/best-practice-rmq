package com.test.demo.model.dto;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * 消息实体
 *
 * @author huangxiaodong
 * @date 2021/5/15  15:26
 */
@Data
public class MsgDto implements Serializable {
    /**
     * 消息Id
     */
    @Size(max = 5, min = 5, message = "参数不合法")
    @JSONField(name = "msg_id")
    String msgId;
    /**
     * 消息内容
     */
    String text;
}
