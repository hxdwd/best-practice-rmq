package com.test.demo.utils.retry;

import com.test.demo.functional.SystemException;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.retry.RetryListener;
import org.springframework.retry.backoff.ExponentialBackOffPolicy;
import org.springframework.retry.policy.SimpleRetryPolicy;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.text.MessageFormat;
import java.time.Duration;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Supplier;

/**
 * 提供一个重试工具类
 * 方便aop相关操作，也可以直接使用@Retryable
 * 不依赖<code> @EnableRetry <code/> 启动
 *
 * @author huangxiaodong
 * @date 2022/1/20  15:41
 */
@Slf4j
@Component
public class RetryEnhanceUtil {

    private RetryListener retryListener;
    private RetryProperties retryProperties;
    /**
     * 缓存重试模板，key是重试次数，value是重试template
     */
    private ConcurrentHashMap<Integer, RetryTemplate> retryTemplateMap = new ConcurrentHashMap<>();

    public <R> R invoke(Supplier<R> operation, int maxAttempts) {
        if (maxAttempts <= 0) {
            throw new IllegalArgumentException("maxAttempts must be greater than 0");
        }
        RetryTemplate retryTemplate = getRetryTemplate(maxAttempts);
        // (RetryCallback<R, SystemException>)
        return retryTemplate.execute(context -> invokeInternal(operation));
    }

    private <R> R invokeInternal(Supplier<R> operation) {
        try {
            return operation.get();
        } catch (Exception e) {
            // 这里捕获所有的异常，可以指定特定异常
            throw new SystemException(MessageFormat.format("操作异常[{0}]", e.getMessage()), e);
        }
    }


    private RetryTemplate getRetryTemplate(int maxAttempts) {
        return retryTemplateMap.computeIfAbsent(maxAttempts, integer -> createRetryTemplate(maxAttempts));
    }

    private RetryTemplate createRetryTemplate(int maxAttempts) {
        // 初始化retryTemplate
        RetryTemplate retryTemplate = new RetryTemplate();

        // 指数级增长的back off策略
        ExponentialBackOffPolicy exponentialBackOffPolicy = new ExponentialBackOffPolicy();
        exponentialBackOffPolicy.setInitialInterval(retryProperties.getInitDelay().toMillis());
        exponentialBackOffPolicy.setMaxInterval(retryProperties.getMaxDelay().toMillis());
        exponentialBackOffPolicy.setMultiplier(retryProperties.getMultiplier());
        retryTemplate.setBackOffPolicy(exponentialBackOffPolicy);

        // 允许重试的异常
        Map<Class<? extends Throwable>, Boolean> policyMap = new HashMap<>(1);
        policyMap.put(SystemException.class, true);
        // 简单重试策略
        SimpleRetryPolicy retryPolicy = new SimpleRetryPolicy(maxAttempts, policyMap);
        retryTemplate.setRetryPolicy(retryPolicy);

        // 注册重试监听器
        if (null != retryListener) {
            retryTemplate.registerListener(retryListener);
        }
        return retryTemplate;
    }

    @Autowired
    public void setRetryListener(RetryListener retryListener) {
        this.retryListener = retryListener;
    }

    @Autowired
    public void setRetryProperties(RetryProperties retryProperties) {
        this.retryProperties = retryProperties;
    }

    @Data
    @Configuration
    @ConfigurationProperties(prefix = "com.business.send.retry")
    static class RetryProperties implements Serializable {
        /**
         * 重试时，初次延迟间隔，默认100毫秒
         */
        private Duration initDelay = Duration.ofMillis(100);
        /**
         * 重试时，最大延迟间隔，默认3秒
         */
        private Duration maxDelay = Duration.ofSeconds(3);
        /**
         * 重试时，延迟时间增加的乘法因子（乘数），越大达到最大延迟间隔的时间越短，不要设置为1
         */
        private Integer multiplier = 3;
    }

}
