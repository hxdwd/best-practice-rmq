package com.test.demo.utils.middleware;

import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.scripting.support.ResourceScriptSource;

import java.time.Duration;
import java.util.Collections;

/**
 * redis 原子操作类 包括lua脚本等
 *
 * @author huangxiaodong
 * @date 2022/2/18  17:51
 */
@Slf4j
public class RedisAtomicHelper {

    private final StringRedisTemplate stringRedisTemplate;
    private DefaultRedisScript<Boolean> strCompareDelScript;

    public RedisAtomicHelper(StringRedisTemplate stringRedisTemplate) {
        this.stringRedisTemplate = stringRedisTemplate;
    }

    public void init() {
        strCompareDelScript = new DefaultRedisScript<>();
        strCompareDelScript.setResultType(Boolean.class);
        strCompareDelScript.setScriptSource(new ResourceScriptSource(
                new ClassPathResource("redis.script/str-compare-del.lua", RedisAtomicHelper.class.getClassLoader())));
    }

    /**
     * 即使异常也能正常返回
     *
     * @return true:设置成功 false：失败
     */
    public Boolean setLockAnyway(String key, String value, Duration timeout) {
        try {
            return stringRedisTemplate.opsForValue().setIfAbsent(key, value, timeout);
        } catch (Exception e) {
            log.error("操作redis-setLockAnyway失败，默认返回", e);
            // 发生异常认为失败
            return Boolean.FALSE;
        }
    }

    /**
     * 原子操作 - 获取key的value值，判断是否相等，相等则删除
     *
     * @param key     key
     * @param compare 比较值
     * @return true:相等并且确保删除，false:值不相同，
     */
    public Boolean compareAndDel(String key, String compare) {
        try {
            return stringRedisTemplate.execute(strCompareDelScript, Collections.singletonList(key), compare);
        } catch (Exception e) {
            log.error("操作redis-compareAndDel失败，默认返回", e);
            // 发生异常认为失败
            return Boolean.FALSE;
        }
    }

}
