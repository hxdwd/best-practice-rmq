package com.test.demo.utils.log;

import java.lang.annotation.*;

/**
 * 日志注解
 *
 * @author huangxiaodong
 * @date 2021/8/11  16:59
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
@Inherited
public @interface LogMethod {

    /**
     * 方法描述
     */
    String desc() default "";

    /**
     * 是否打印方法耗时
     */
    boolean showTimeConsuming() default true;
}
