package com.test.demo.utils.log;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.MDC;

import java.util.UUID;

/**
 * 日志上下文工具
 *
 * @author huangxiaodong
 * @date 2022/1/14  9:38
 */
public class LogContextUtils {

    private LogContextUtils() {
    }

    /**
     * 获取线程上下文中的日志跟踪ID
     *
     * @return 日志跟踪ID
     */
    public static String logId() {
        String logId = MDC.get("X-B3-TraceId");
        if (StringUtils.isBlank(logId)) {
            logId = gen();
        }
        return logId;
    }

    private static String gen(){
        return UUID.randomUUID().toString().replace("-", "");
    }
}
