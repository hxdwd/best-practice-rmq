package com.test.demo.utils;

import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.function.Supplier;

/**
 * 线程等待小工具
 *
 * @author huangxiaodong
 * @date 2022/2/18  10:57
 */
public class TimeSpinUtils {

    private static final Logger log = LoggerFactory.getLogger(TimeSpinUtils.class);

    private TimeSpinUtils() {
        throw new IllegalStateException("Utility class.");
    }

    /**
     * 不断获取结果 直到超时
     *
     * @param maxTimeout 最大的等待时间
     * @param times      等待次数
     * @param operation  当前操作
     * @param wanted     期望值
     */
    public static <T> void getWantedPolling(long maxTimeout, int times, Supplier<T> operation, T wanted) {
        long spinMills = maxTimeout / times;
        for (int i = 1; i < times; i++) {
            log.info("自旋第{}次尝试获取,已经自旋：{}ms", i, i * spinMills);
            spin(spinMills);
            if (wanted.equals(operation.get())) {
                log.info("期望结果出现，自旋提前结束");
                break;
            }
        }
    }

    /**
     * 自旋
     *
     * @param millis 自旋时间，毫秒
     */
    public static void spin(long millis) {
        long start = System.currentTimeMillis();
        boolean isInterrupted = false;
        while (System.currentTimeMillis() - start < millis && !isInterrupted) {
            if (Thread.interrupted()) {
                isInterrupted = true;
            }
        }
    }

    public static void main(String[] args) {
        Car car = new Car();
        log.info("{}", JSON.toJSONString(car));
    }




}
