package com.test.demo.utils.notice;

import java.lang.annotation.*;

/**
 * 表变动通知
 *
 * @author huangxiaodong
 * @date 2021/11/25  16:36
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Notice {
    /**
     * 变更类型：add/update/delete
     */
    String modifyType() default "update";
}
