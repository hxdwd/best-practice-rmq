package com.test.demo.service.circular.dependencies;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 循环依赖2
 *
 * @author huangxiaodong
 * @date 2022/2/15  14:00
 */
@Service
public class TestService2 {

    @Autowired
    private TestService6 testService6;

    public void test() {
        System.out.println("test2");
    }

    public static void main(String[] args) {
        System.out.println("cst".startsWith("2"));
    }
}
