package com.test.demo.service.circular.dependencies;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

/**
 * 循环依赖1
 *
 * <code>
 *     org.springframework.beans.factory.support.DefaultSingletonBeanRegistry#getSingleton(java.lang.String)
 * </code>
 *
 * @author huangxiaodong
 * @date 2022/2/15  14:00
 */
@Service
public class TestService6 {

    @Autowired
    private TestService2 testService2;

    @Async
    public void test() {
        System.out.println("test1");
    }
}
