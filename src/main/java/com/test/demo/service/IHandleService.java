package com.test.demo.service;

import com.test.demo.model.dto.MsgDto;

/**
 * 处理消息 接口类
 *
 * @author huangxiaodong
 * @date 2021/5/15  15:07
 */
public interface IHandleService {

    /**
     * 处理消息
     *
     * @param msg 初始消息
     */
    void handle(MsgDto msg);
}
