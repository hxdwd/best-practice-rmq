package com.test.demo.service.test;

import com.aliyun.aligenieiap_1_0.Client;
import com.aliyun.aligenieiap_1_0.models.PushNotificationsRequest;
import com.aliyun.teaopenapi.models.Config;

import java.util.HashMap;
import java.util.Map;

public class Sample {
    /**
     * 使用AK&SK初始化账号Client
     *
     * @param accessKeyId     /
     * @param accessKeySecret /
     * @return Client
     * @throws Exception
     */
    public static Client createClient(String accessKeyId, String accessKeySecret) throws Exception {
        Config config = new Config()
                // 您的AccessKey ID
                .setAccessKeyId(accessKeyId)
                // 您的AccessKey Secret
                .setAccessKeySecret(accessKeySecret);
        // 访问的域名
        config.endpoint = "openapi.aligenie.com";
        return new Client(config);
    }

    public static void main(String[] args) throws Exception {

        Map<String, String> paramsMap = new HashMap<>();
        paramsMap.put("name", "value");

        com.aliyun.aligenieiap_1_0.Client client = Sample.createClient("accessKeyId", "accessKeySecret");
        PushNotificationsRequest pushRequest = new PushNotificationsRequest()
                .setNotificationUnicastRequest(new PushNotificationsRequest.PushNotificationsRequestNotificationUnicastRequest()
                        .setEncodeKey("xxx")
                        // 发送目标
                        .setSendTarget(new PushNotificationsRequest.PushNotificationsRequestNotificationUnicastRequestSendTarget()
                                .setTargetIdentity("xxx")
                                .setTargetType("xx"))
                        // 实际消息体
                        .setPlaceHolder(paramsMap)
                        // 消息模板Id
                        .setMessageTemplateId("templateId"));

        // 复制代码运行请自行打印 API 的返回值
        client.pushNotifications(pushRequest);
    }
}
