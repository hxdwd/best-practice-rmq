package com.test.demo.service.utils;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.rocketmq.client.producer.SendCallback;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.client.producer.SendStatus;
import org.apache.rocketmq.common.message.MessageConst;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.slf4j.Logger;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;

import java.text.MessageFormat;

/**
 * rocketMq 工具类
 *
 * @author huangxiaodong
 * @date 2021/1/14  16:01
 */
@Slf4j
@Component
public class RocketMqUtil {

    private final RocketMQTemplate rocketMqTemplate;

    public RocketMqUtil(RocketMQTemplate rocketMqTemplate) {
        this.rocketMqTemplate = rocketMqTemplate;
    }

    /**
     * @param receiptMsg 发送内容
     * @param msgTopic   消息队列
     * @param msgDesc    消息行为描述
     */
    public void asyncSendMsg(String receiptMsg, String msgTopic, String msgDesc) {
        // 获取msgId，可能为空
        String msgId = (String) JSON.parseObject(receiptMsg).get("msg_id");
        Message<String> rocketMsg = StringUtils.isNotBlank(msgId) ?
                MessageBuilder.withPayload(receiptMsg).setHeaderIfAbsent(MessageConst.PROPERTY_KEYS, msgId).build()
                : MessageBuilder.withPayload(receiptMsg).build();

        log.info("[{}]发送消息: {}", msgDesc, JSON.toJSONString(rocketMsg));
        try {
            // 流水发送使用默认的 producer group, 非主流程功能, 异步rocket mq发送
            rocketMqTemplate.asyncSend(msgTopic, rocketMsg,
                    new RocketMqSendCallback(msgId, "消息发送", log));
        } catch (Exception e) {
            log.error("消息发送异常", e);
        }
    }

    /**
     * rocket mq回调
     */
    static class RocketMqSendCallback implements SendCallback {
        /**
         * 消息ID
         */
        private String msgId;
        /**
         * 操作描述
         */
        private String opDesc;
        protected Logger log;

        public RocketMqSendCallback(String msgId, String opDesc, Logger log) {
            this.msgId = (null == msgId ? "为空" : msgId);
            this.opDesc = opDesc;
            this.log = log;
        }

        @Override
        public void onSuccess(SendResult sendResult) {
            if (log.isDebugEnabled()) {
                log.debug("msgId({}){} 返回: {}", msgId, opDesc, JSON.toJSONString(sendResult));
            }
            log.info("msgId({}){} {}", msgId, opDesc, SendStatus.SEND_OK == sendResult.getSendStatus() ? "成功" : "失败");
        }

        @Override
        public void onException(Throwable e) {
            if (log.isErrorEnabled()) {
                log.error(MessageFormat.format("msgId({0}){1} 异常", msgId, opDesc), e);
            }
        }
    }
}
