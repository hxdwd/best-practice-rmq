package com.test.demo.service.utils;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * 提供redis相关操作接口
 *
 * @author huangxiaodong
 * @date 2021/6/21  17:14
 */
@Slf4j
@Service
public class RedisUtil {

    private RedisTemplate<Object, Object> redisTemplate;

    // 对string的操作

    /**
     * 普通的set
     *
     * @param key   key名字
     * @param value 值
     */
    public void setValue(String key, String value) {
        redisTemplate.opsForValue().set(key, value);
    }

    /**
     * 带返回的set，常用于防重/互斥更新等
     *
     * @param key     key
     * @param timeout 过期时间 s
     * @return true：可以操作 false：已经有值，不可操作
     */
    public Boolean setValueIfAbsent(String key, Integer timeout) {
        return redisTemplate.opsForValue().setIfAbsent(key, "1", timeout, TimeUnit.SECONDS);
    }

    // 对hash的操作

    /**
     * set hash，一个hkey的那种
     *
     * @param key   key
     * @param hkey  hkey
     * @param value value for khey
     */
    public void setHash(String key, String hkey, String value) {
        redisTemplate.opsForHash().put(key, hkey, value);
    }

    /**
     * set 全量hash
     *
     * @param key key
     * @param map 参数
     */
    public void setAllHash(String key, Map<String, Object> map) {
        redisTemplate.opsForHash().putAll(key, map);
    }

    /**
     * 增量某个hkey，原子操作
     * 这里可以key可以不存在，直接set
     *
     * @param key  key
     * @param hkey hkey
     * @param num  操作数量，可任意值
     */
    public void setOneHkey(String key, String hkey, Long num) {
        redisTemplate.opsForHash().increment(key, hkey, num);
    }

    /**
     * 删除key
     *
     * @param key key
     * @return true：成功 false:失败
     */
    public Boolean del(String key) {
        return redisTemplate.delete(key);
    }

    @Autowired
    public void setRedisTemplate(RedisTemplate<Object, Object> redisTemplate) {
        this.redisTemplate = redisTemplate;
    }
}
