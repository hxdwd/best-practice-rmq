package com.test.demo.service.schedule;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.*;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.client.RestTemplate;

import static org.springframework.http.MediaType.APPLICATION_FORM_URLENCODED_VALUE;

/**
 * 定时任务
 *
 * @author huangxiaodong
 * @date 2022/1/17  9:14
 */
@Slf4j
@Configuration
@EnableScheduling
public class ScheduleTask {

    private final RestTemplate restTemplate;

    public ScheduleTask(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Scheduled(cron = "0 0/30 10-23 * * ?")
    public void schedule() {
        long start = System.currentTimeMillis();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.parseMediaType(APPLICATION_FORM_URLENCODED_VALUE));
        HttpEntity<String> reqEntity = new HttpEntity<>(headers);

        ResponseEntity<String> tokenResp = restTemplate.exchange("http://172.20.6.6:30437/token?prodType=weixin&appId=wxe962bdb469d99ca6",
                HttpMethod.GET,
                reqEntity, String.class);

        String token = JSON.parseObject(tokenResp.getBody()).getString("accessToken");

        String ticketRespWx = restTemplate.getForObject("https://api.weixin.qq.com/cgi-bin/ticket/getticket?offset_type=1&access_token=" + token + "&type=jsapi",
                String.class);
        log.info("微信原生响应：{}，耗时：{}ms", ticketRespWx, System.currentTimeMillis() - start);

        long start1 = System.currentTimeMillis();
        ResponseEntity<String> resp = restTemplate.exchange("http://172.20.6.6:30437/ticket?appId=wxe962bdb469d99ca6&type=jsapi",
                HttpMethod.GET,
                reqEntity, String.class);

        log.info("中控响应：{}，耗时：{}ms", resp.getBody(), System.currentTimeMillis() - start1);
    }


}
