package com.test.demo.service;

import com.test.demo.model.dto.MsgDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Random;

/**
 * 处理消息 实现类
 *
 * @author huangxiaodong
 * @date 2021/5/15  15:10
 */
@Slf4j
@Service
public class HandleServiceImpl implements IHandleService {

    @Override
    public void handle(MsgDto msg) {
        return;
    }

    public static void main(String[] args) {
        System.out.println(Integer.toBinaryString(-1));
    }
}
