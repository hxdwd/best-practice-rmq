package com.test.demo.controller;

import com.test.demo.model.dto.vo.RespVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.validation.ConstraintViolationException;

/**
 * 统一全局controller 捕获
 *
 * @author huangxiaodong
 * @date 2021/7/23  11:16
 */
@Slf4j
@ControllerAdvice
public class OverAllControllerExHandler {
    @ExceptionHandler(Throwable.class)
    @ResponseBody
    public RespVO exHandler(Throwable e) throws Throwable {
        // 参数异常
        if (e instanceof IllegalArgumentException|| e instanceof ConstraintViolationException) {
            return RespVO.builder().retCode("99").retMsg(e.getMessage()).build();
        }
        throw e;
    }

}

