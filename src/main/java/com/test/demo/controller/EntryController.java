package com.test.demo.controller;

import com.test.demo.model.dto.MsgDto;
import com.test.demo.utils.log.LogMethod;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * 入口controller
 *
 * @author hxd
 * @date 2021/7/17  15:19
 */
@Slf4j
@Validated
@RestController
public class EntryController {

    @PostMapping(value = "/test", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Object getHello(@RequestBody String reqBody) {
        log.info(reqBody);
        return "hello";
    }

    @GetMapping(value = "/get")
    public Object testGet(@RequestParam Map<String, String> params) {
        return params;
    }

    @LogMethod(desc = "entry")
    @PostMapping(value = "/post")
    public Object testPost(@RequestBody @Validated MsgDto msgDto) {
        return msgDto;
    }
}
