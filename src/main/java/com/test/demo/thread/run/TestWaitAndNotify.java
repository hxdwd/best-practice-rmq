package com.test.demo.thread.run;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * test for keyword wait&notify
 *
 * @author huangxiaodong
 * @date 2021/8/24  10:43
 */
public class TestWaitAndNotify {

    public static final Object object = new Object();

    public static void main(String[] args) {
        ThreadPoolExecutor executor = new ThreadPoolExecutor(100, 100, 10000,
                TimeUnit.MILLISECONDS, new LinkedBlockingQueue<>(2), new ThreadPoolExecutor.CallerRunsPolicy());


        Thread thread1 = new Thread(() -> {
            synchronized (object) {
                try {
                    object.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("线程" + Thread.currentThread().getName() + "获取到了锁\n");
            }
        });


        Thread thread2 = new Thread(() -> {
            synchronized (object) {
                object.notify();
                System.out.println("线程" + Thread.currentThread().getName() + "调用了object.notify()");
            }
            System.out.println("线程" + Thread.currentThread().getName() + "释放了锁");
        });

        for (int i = 0; i < 50; i++) {
            executor.execute(thread2);
            executor.execute(thread1);
        }

    }
}
