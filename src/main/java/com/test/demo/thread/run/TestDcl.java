package com.test.demo.thread.run;

import lombok.extern.slf4j.Slf4j;

import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
 * 测试DCL问题
 *
 * @author huangxiaodong
 * @date 2021/9/26  17:24
 */
@Slf4j
public class TestDcl {

    private static TestDcl instance;
    public int test;

    private TestDcl() {
        try {
            TimeUnit.MILLISECONDS.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        log.info("初始化一次");
        this.test = new Random().nextInt(200) + 1;
    }

    public static TestDcl getInstance() {
        if (null == instance) {
            synchronized (TestDcl.class) {
                if (null == instance) {
                    instance = new TestDcl();
                }
            }
        }
        return instance;
    }

    public int getTest() {
        return test;
    }
}
