package com.test.demo.thread.run.test;

import com.test.demo.thread.run.TestCallable;
import com.test.demo.thread.run.TestRunnable;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.client.utils.DateUtils;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFuture;

import java.util.Date;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

/**
 * 测试类
 *
 * @author huangxiaodong
 * @date 2021/8/11  17:06
 */
@Slf4j
@Service
public class Run {

    private final ThreadPoolTaskExecutor taskExecutor;

    public Run(ThreadPoolTaskExecutor taskExecutor) {
        this.taskExecutor = taskExecutor;
    }

    public void runRunnable() {
        taskExecutor.execute(new TestRunnable("Runnable，我来了"));
    }

    public void runCallable() {
        log.info("当前时间：{}", DateUtils.formatDate(new Date(), "yyyy-MM-dd HH:mm:ss"));
        Future<String> res = taskExecutor.submit(new TestCallable("Callable，我来了"));
        try {
            log.info("获取结果：{}", res.get());
            log.info("main线程处理其他事情");
            TimeUnit.SECONDS.sleep(1);
            log.info("当前时间：{}", DateUtils.formatDate(new Date(), "yyyy-MM-dd HH:mm:ss"));
        } catch (Exception e) {
            log.error("错误", e);
        }
    }

    public void runListenableFuture() {
        log.info("当前时间：{}", DateUtils.formatDate(new Date(), "yyyy-MM-dd HH:mm:ss"));
        ListenableFuture<String> listener = taskExecutor.submitListenable(new TestCallable("Callable，我来了"));
        try {
            listener.addCallback(result -> log.info("获取结果：", result),
                    ex -> log.error("错误，", ex));
            log.info("main线程处理其他事情");
            TimeUnit.SECONDS.sleep(1);
            log.info("当前时间：{}", DateUtils.formatDate(new Date(), "yyyy-MM-dd HH:mm:ss"));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void runGetRunnable() {
        String params = "Runnable,我要获取结果";
        Future<String> res = taskExecutor.getThreadPoolExecutor().submit(new TestRunnable(params), "这是个结果");
        try {
            log.info("获取结果：{}", res.get());
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

    }
}
