package com.test.demo.thread.run;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.Callable;

/**
 * test for Callable
 *
 * @author huangxiaodong
 * @date 2021/8/11  17:18
 */
@Slf4j
public class TestCallable implements Callable<String> {

    private String params;

    public TestCallable(String params) {
        this.params = params;
    }

    @Override
    public String call() {
        log.info("启动Callable的run方法，入参：{}", params);
        if (true) {
            throw new RuntimeException("手动抛出Callable异常");
        }
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            log.error("操作失败，", e);
        }
        return params;
    }
}
