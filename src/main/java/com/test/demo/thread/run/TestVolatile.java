package com.test.demo.thread.run;

import lombok.Synchronized;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * test for volatile
 *
 * @author huangxiaodong
 * @date 2021/8/19  17:50
 */
public class TestVolatile {
    public int inc = 0;
    Lock lock = new ReentrantLock();
    ThreadLocal<String> threadLocal = new ThreadLocal<>();


    public void increase() {
        inc++;
    }

    public void increaseAtomic() {
        lock.lock();
        try {
            inc++;
        } finally{
            lock.unlock();
        }
    }

    public static void main(String[] args) {
        final TestVolatile test = new TestVolatile();
        for (int i = 0; i < 10; i++) {
            new Thread(() -> {
                for (int j = 0; j < 1000; j++) {
                    test.increaseAtomic();
                }
            }).start();
        }
        // 保证前面的线程都执行完
        while (Thread.activeCount() > 2) {
            Thread.yield();
        }
        System.out.println(test.inc);
    }
}
