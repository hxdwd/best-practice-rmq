package com.test.demo.thread.run;

import lombok.extern.slf4j.Slf4j;

/**
 * test for Runnable
 *
 * @author huangxiaodong
 * @date 2021/8/11  17:02
 */
@Slf4j
public class TestRunnable implements Runnable {
    /**
     * 入口参数
     */
    private String params;

    public TestRunnable(String params) {
        this.params = params;
    }

    @Override
    public void run() {
        log.info("启动Runnable的run方法，入参：{}", params);
        if (true) {
            throw new RuntimeException("手动抛出异常");
        }
        params = "我改变了";
    }
}
