package com.test.demo.thread.run;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.Semaphore;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * test for Semaphore
 *
 * @author huangxiaodong
 * @date 2021/8/26  17:26
 */
@Slf4j
public class TestSemaphore {


    public static void main(String[] args) {
        int num = 5;
        Semaphore semaphore = new Semaphore(num);
        ThreadPoolExecutor executor = new ThreadPoolExecutor(100, 100, 10000,
                TimeUnit.MILLISECONDS, new LinkedBlockingQueue<>(2), new ThreadPoolExecutor.CallerRunsPolicy());
        for (int i = 0; i < num + 3; i++) {
            int finalI = i;
            executor.execute(() -> {
                try {
                    if (semaphore.tryAcquire(100, TimeUnit.MILLISECONDS)) {
                        log.info("抢占成功，执行第{}个任务", finalI);
                        TimeUnit.MILLISECONDS.sleep(1000);
                        log.info("执行第{}个任务，任务释放", finalI);
                        semaphore.release();
                    } else {
                        log.info("抢占失败，不执行第{}个任务", finalI);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
        }
    }
}
