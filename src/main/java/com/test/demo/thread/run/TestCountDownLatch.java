package com.test.demo.thread.run;

import lombok.extern.slf4j.Slf4j;

import java.sql.Timestamp;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * test for keyword CountDownLatch
 *
 * @author huangxiaodong
 * @date 2021/8/26  17:02
 */
@Slf4j
public class TestCountDownLatch {

    public static void main(String[] args) {
        int taskNum = 10;
        CountDownLatch latch = new CountDownLatch(taskNum);

        ThreadPoolExecutor executor = new ThreadPoolExecutor(2, 5, 0,
                TimeUnit.MILLISECONDS, new LinkedBlockingQueue<>(10), new ThreadPoolExecutor.CallerRunsPolicy());

        for (int i = 0; i < taskNum; i++) {
            int finalI = i;
            executor.execute(() -> {
                log.info("执行第{}个任务", finalI);
                latch.countDown();
            });
        }


        try {
            latch.await();
            executor.execute(() -> log.info("执行最后的任务"));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
