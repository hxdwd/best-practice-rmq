package com.test.demo.config;

import com.alibaba.fastjson.serializer.SerializerFeature;
import com.alibaba.fastjson.support.config.FastJsonConfig;
import com.alibaba.fastjson.support.spring.FastJsonHttpMessageConverter;
import org.springframework.boot.autoconfigure.http.HttpMessageConverters;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.ArrayList;
import java.util.List;

/**
 * mvc采用fastJson序列化
 *
 * @author huangxiaodong
 * @date 2021/7/23  10:15
 */
@Configuration
public class FastJsonHttpMsgConvertConfig {

    @Bean
    public FastJsonHttpMessageConverter fastJsonHttpMsgConverter() {
        // 创建FastJson信息转换对象
        FastJsonHttpMessageConverter fastConverter = new FastJsonHttpMessageConverter();
        // 创建FastJsonConfig对象并设定序列化规则
        FastJsonConfig fastJsonConfig = new FastJsonConfig();
        fastJsonConfig.setSerializerFeatures(
                // 格式化json
                SerializerFeature.PrettyFormat,
                // null字符串转换为字符串
                SerializerFeature.WriteNonStringKeyAsString,
                // 消除对同一对象循环引用的问题
                SerializerFeature.DisableCircularReferenceDetect);
        // 将转换规则应用于转换对象
        fastConverter.setFastJsonConfig(fastJsonConfig);
        // 中文乱码解决方案
        List<MediaType> fastMediasTypes = new ArrayList<>();
        // 设定Json格式且编码为utf-8
        fastMediasTypes.add(MediaType.APPLICATION_JSON_UTF8);
        fastConverter.setSupportedMediaTypes(fastMediasTypes);
        return fastConverter;
    }

    @Bean
    public HttpMessageConverters fastJsonHttpMessageConverters() {
        return new HttpMessageConverters(fastJsonHttpMsgConverter());
    }
}
