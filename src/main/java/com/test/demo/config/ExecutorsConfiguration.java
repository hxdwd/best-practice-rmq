package com.test.demo.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.io.Serializable;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * 线程池配置
 *
 * @author huangxiaodong
 * @since 2020/8/20
 */
@Configuration
@EnableConfigurationProperties(ExecutorsConfiguration.MsgReceivePoolProperties.class)
public class ExecutorsConfiguration {

    private final MsgReceivePoolProperties poolProperties;

    public ExecutorsConfiguration(MsgReceivePoolProperties poolProperties) {
        this.poolProperties = poolProperties;
    }

    @Bean
    public ThreadPoolTaskExecutor taskExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(poolProperties.getCoreSize());
        executor.setMaxPoolSize(poolProperties.getMaxSize());
        executor.setQueueCapacity(poolProperties.getQueueSize());
        executor.setKeepAliveSeconds(poolProperties.getKeepAliveSeconds());
        executor.setAllowCoreThreadTimeOut(poolProperties.getAllowCoreThreadTimeOut());
        executor.setThreadNamePrefix(poolProperties.getThreadNamePrefix());

        // 拒绝策略，主线程直接调用
        executor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());

        return executor;
    }

    @Data
    @ConfigurationProperties(prefix = "com.framework.pool")
    public static class MsgReceivePoolProperties implements Serializable {
        /**
         * 消息处理线程池-核心线程数
         */
        private int coreSize = 5;
        /**
         * 消息处理线程池-最大线程数
         */
        private int maxSize = 20;

        /**
         * 消息处理线程池-队列容量
         */
        private int queueSize = 200;

        /**
         * 空闲线程keep alive时间，单位秒
         */
        private int keepAliveSeconds = 600;

        /**
         * 是否允许核心线程超时
         */
        private Boolean allowCoreThreadTimeOut = Boolean.FALSE;

        /**
         * 线程名称前缀
         */
        private String threadNamePrefix = "async-work-";
    }
}
