package com.test.demo.config;


import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.ssl.SSLContexts;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.client.RestTemplate;

import javax.net.ssl.SSLContext;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;

/**
 * http restTemplate支持
 *
 * @author huangxiaodong
 * @date 2022/1/17  9:40
 */
@Slf4j
@Configuration
@EnableConfigurationProperties(value = {RestTemplateConfig.RestTemplateProperties.class})
public class RestTemplateConfig {

    @Bean
    public RestTemplate restTemplate(RestTemplateProperties prop) throws KeyStoreException, NoSuchAlgorithmException, KeyManagementException {
        return new RestTemplate(new HttpComponentsClientHttpRequestFactory(httpClient(prop)));
    }

    private HttpClient httpClient(RestTemplateProperties prop) throws KeyStoreException, NoSuchAlgorithmException, KeyManagementException {

        Registry<ConnectionSocketFactory> registry = RegistryBuilder.<ConnectionSocketFactory>create()
                .register("http", PlainConnectionSocketFactory.getSocketFactory())
                .register("https", trustAllSslConnSocketFactory())
                .build();
        PoolingHttpClientConnectionManager connectionManager = new PoolingHttpClientConnectionManager(registry);

        connectionManager.setMaxTotal(prop.getMaxTotal());

        connectionManager.setDefaultMaxPerRoute(prop.getDefaultMaxPerRoute());
        RequestConfig requestConfig = RequestConfig.custom()
                .setSocketTimeout(prop.getSocketTimeout())
                .setConnectTimeout(prop.getConnectTimeout())
                .setConnectionRequestTimeout(prop.getConnectionRequestTimeout())
                .build();
        return HttpClientBuilder.create()
                .setDefaultRequestConfig(requestConfig)
                .setConnectionManager(connectionManager)
                .build();
    }

    private SSLConnectionSocketFactory trustAllSslConnSocketFactory() throws NoSuchAlgorithmException, KeyManagementException, KeyStoreException {
        // 信任策略，信任全部
        TrustStrategy trustStrategy = (X509Certificate[] chain, String authType) -> true;
        SSLContext sslContext = SSLContexts.custom().loadTrustMaterial(null, trustStrategy).build();
        return new SSLConnectionSocketFactory(sslContext, NoopHostnameVerifier.INSTANCE);
    }

    @Data
    @ConfigurationProperties(value = "com.framework.http.client")
    static class RestTemplateProperties implements Serializable {
        /**
         * 设置整个连接池最大连接数 根据自己的场景决定
         */
        private int maxTotal = 100;
        /**
         * 路由是对maxTotal的细分
         */
        private int defaultMaxPerRoute = 100;
        /**
         * 毫秒; 服务器返回数据(response)的时间，超过该时间抛出read timeout
         */
        private int socketTimeout = 3000;
        /**
         * 毫秒; 连接上服务器(握手成功)的时间，超出该时间抛出connect timeout
         */
        private int connectTimeout = 1000;
        /**
         * 毫秒; 从连接池中获取连接的超时时间，超过该时间未拿到可用连接，会抛出
         * org.apache.http.conn.ConnectionPoolTimeoutException: Timeout waiting for connection from pool
         */
        private int connectionRequestTimeout = 10;
    }
}
