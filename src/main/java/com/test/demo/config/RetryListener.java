package com.test.demo.config;


import lombok.extern.slf4j.Slf4j;
import org.springframework.retry.RetryCallback;
import org.springframework.retry.RetryContext;
import org.springframework.retry.listener.RetryListenerSupport;
import org.springframework.stereotype.Component;

import javax.print.attribute.standard.JobSheets;

/**
 * spring retry重试日志
 *
 * @author huangxiaodong
 * @date 15:32 2020/10/9
 */
@Slf4j
@Component
public class RetryListener extends RetryListenerSupport {

    @Override
    public <T, E extends Throwable> void onError(RetryContext context, RetryCallback<T, E> callback, Throwable throwable) {
        String targetMethodName = (String) context.getAttribute(RetryContext.NAME);
        log.info("Spring retry: {}-第{}次处理失败, 原因: {}", targetMethodName, context.getRetryCount(), throwable.getMessage());
        super.onError(context, callback, throwable);
    }
}
