package com.test.demo.service.utils;


import com.test.demo.Application;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

/**
 * test for redisUtil
 *
 * @author huangxiaodong
 * @date 2021/7/1  16:36
 */
@DisplayName("RedisTemplate测试")
@Slf4j
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = Application.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class RedisUtilTest {

    @Autowired
    private RedisUtil redisUtil;

    String key = "hxd";

    @DisplayName("测试普通set值")
    @Test
    void setValue() {
        redisUtil.setValue(key, "test");

        // 删除测试数据
        redisUtil.del(key);
        Assertions.assertTrue(true);
    }

    @DisplayName("测试set if absent")
    @Test
    void setValueIfAbsent() {
        String key = "antiKey";
        int timeout = 5;
        Assertions.assertTrue(redisUtil.setValueIfAbsent(key, timeout));
        // 第二次set值应该失败
        Assertions.assertFalse(redisUtil.setValueIfAbsent(key, 5));
    }

    @Test
    void setHash() {
        redisUtil.setHash(key, "khey", "1");

        // 清除数据
        Assertions.assertTrue(redisUtil.del(key));

    }

    public static String getIdcFromServerIp(String serverIp) {
        Map<Integer, String> matchRuleMap = new HashMap<>();
        matchRuleMap.put(255, "龙岗");
        matchRuleMap.put(128, "金山");
        matchRuleMap.put(63, "科兴");
        String matchRule = "0-63:科兴;64-127:金山;128-255:龙岗";

        int thirdSet = Integer.parseInt(serverIp.split("[.]")[2]);
        System.out.println(thirdSet);
        AtomicReference<String> realIdc = new AtomicReference<>("idc");
        matchRuleMap.forEach((divide, idc) -> {
            if (thirdSet < divide) {
                realIdc.set(idc);
            }
        });
        return realIdc.get() + "-" + serverIp;
    }

}