package com.test.demo.utils.middleware;

import com.test.demo.Application;
import com.test.demo.utils.TimeSpinUtils;
import com.test.demo.utils.log.LogContextUtils;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.Duration;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * 分布式锁测试
 *
 * @author huangxiaodong
 * @date 2022/2/18  18:00
 */
@DisplayName("redis分布式锁测试")
@Slf4j
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = Application.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class RedisAtomicHelperTest {
    @Autowired
    private RedisAtomicHelper redisAtomicHelper;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;


    @SneakyThrows
    @Test
    void test() {
        ThreadPoolExecutor poolExecutor = (ThreadPoolExecutor) Executors.newFixedThreadPool(3);
        poolExecutor.execute(() -> getLock("key"));
        poolExecutor.execute(() -> getLock("key"));
        poolExecutor.execute(() -> getLock("key"));

        TimeUnit.SECONDS.sleep(4);
    }



    private void getLock(String key) {
        String lockValue = LogContextUtils.logId();
        if (Boolean.TRUE.equals(redisAtomicHelper.setLockAnyway(key, lockValue, Duration.ofSeconds(3)))) {
            try {
                log.info("get lock successfully, do something");
            } finally {
                // 当前线程占有锁才会释放锁
                // 避免场景：获取ticket锁后执行时间过长导致finally内释放了ticket锁
                log.info("手动释放锁,key:{}, result:{}", key,
                        Boolean.TRUE.equals(redisAtomicHelper.compareAndDel(key, lockValue)) ? "成功" : "失败");
            }
        } else {
            log.info("get lock fail, waiting");
            TimeSpinUtils.getWantedPolling(3000L, 10, () -> stringRedisTemplate.hasKey("1111"), Boolean.TRUE);
        }
    }
}