package com.test.demo.utils.retry;

import com.test.demo.Application;
import com.test.demo.functional.SystemException;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.client.utils.DateUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("retry测试")
@Slf4j
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = Application.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class RetryEnhanceUtilTest {

    @Autowired
    private RetryEnhanceUtil retryEnhanceUtil;
    @Autowired
    private TestRetryService testRetryService;

    @Test
    void invoke() {
        Assertions.assertThrows(SystemException.class,
                () -> retryEnhanceUtil.invoke(() -> exAnyway("test"), 3));
    }

    @DisplayName("注解形式")
    @Test
    void testAno() {
        Assertions.assertThrows(RuntimeException.class, () -> testRetryService.throwAnyway());
    }

    private String exAnyway(String param) {
        log.info("入口参数：{}，当前时间：{}", param, DateUtils.formatDate(new Date(), "yyyy-MM-dd HH:mm:ss.SSS"));
        throw new RuntimeException("throw exception anyway");
    }
}