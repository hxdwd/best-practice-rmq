package com.test.demo.utils.retry;

import com.test.demo.thread.run.test.Run;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Service;

/**
 * retry test service
 *
 * @author huangxiaodong
 * @date 2022/1/20  16:18
 */
@Service
public class TestRetryService {

    @Retryable(value = RuntimeException.class, maxAttemptsExpression = "${com.business.send.retry.max-attempts}",
            backoff = @Backoff(delayExpression = "${com.business.send.retry.init-delay}",
                    maxDelayExpression = "${com.business.send.retry.max-delay}",
                    multiplierExpression = "${com.business.send.retry.multiplier}"))
    public void throwAnyway() {
        throw new RuntimeException();
    }
}
