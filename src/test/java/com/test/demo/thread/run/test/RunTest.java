package com.test.demo.thread.run.test;

import com.test.demo.Application;
import com.test.demo.thread.run.TestDcl;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.concurrent.*;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Runnable测试")
@Slf4j
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = Application.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class RunTest {

    @Autowired
    private Run run;

    @Test
    void runTest() {
        run.runRunnable();
        Assertions.assertTrue(true);
    }

    @Test
    void runCallable() {
        run.runCallable();
        Assertions.assertTrue(true);
    }

    @SneakyThrows
    @Test
    void runListenableFuture() {
        run.runListenableFuture();
        TimeUnit.SECONDS.sleep(5);
        Assertions.assertTrue(true);
    }

    @Test
    void runGetRunnable() {
        run.runGetRunnable();
        Assertions.assertTrue(true);
    }


    @SneakyThrows
    @Test
    void runTestThreadPoolExecutor() {
        ThreadPoolExecutor executor = new ThreadPoolExecutor(1, 2, 10,
                TimeUnit.MILLISECONDS, new LinkedBlockingQueue<>(2), new ThreadPoolExecutor.CallerRunsPolicy());
        for (int i = 1; i < 10; i++) {
            int finalI = i;
            executor.execute(() -> {
                log.info("第{}个任务执行,当前线程数：{}", finalI, executor.getActiveCount());
                log.info("第{}个任务执行,当前线程池数：{}", finalI, executor.getPoolSize());
                try {
                    TimeUnit.MILLISECONDS.sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            });
        }
        TimeUnit.SECONDS.sleep(5);
    }

    @Test
    void testError() {
        ExecutorService executorService = Executors.newFixedThreadPool(1);
        Future<Integer> future = executorService.submit(() -> {
            int i = 1 / 0;
            return 1;
        });
        try {
            future.get();
        } catch (Exception e) {
            log.info("错误，", e);
        }
    }

    @SneakyThrows
    @Test
    void testDcl() {
        ExecutorService threadPool = Executors.newFixedThreadPool(100);
        for (int i = 0; i < 100; i++) {
            threadPool.execute(() -> {
                log.info("test's value:{}", TestDcl.getInstance().test);
            });
        }

        TimeUnit.SECONDS.sleep(5);
    }
}