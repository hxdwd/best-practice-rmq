package com.hxd.demo.chain.of.responsibility.outer;

import com.hxd.demo.chain.of.responsibility.model.TestDTO;
import lombok.Builder;
import lombok.Data;

/**
 * 外部控制 接口类
 *
 * @author huangxiaodong
 * @date 2021/8/25  9:41
 */
public interface IChainHandler {
    /**
     * 处理
     *
     * @param testDTO 实体
     * @return 处理结果
     */
    Result handle(TestDTO testDTO);

    @Data
    @Builder
    class Result {
        private String retCode;
        private String retMsg;
    }
}
