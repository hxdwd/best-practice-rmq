package com.hxd.demo.chain.of.responsibility.inner.chain;

import com.hxd.demo.chain.of.responsibility.inner.AbstractChainHandler;
import com.hxd.demo.chain.of.responsibility.model.TestDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * first step to handle chain
 *
 * @author huangxiaodong
 * @date 2021/8/24  17:50
 */
@Slf4j
@Service
public class FirstHandler extends AbstractChainHandler<TestDTO> {

    public FirstHandler(SecondHandler secondHandler) {
        this.nextHandler = secondHandler;
    }

    @Override
    protected void handleRequest(TestDTO msg) {
        log.info("第一步处理，当前实体：{}", msg.getName());
    }
}
