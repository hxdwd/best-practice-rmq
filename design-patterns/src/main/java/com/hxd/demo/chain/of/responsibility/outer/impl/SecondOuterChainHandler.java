package com.hxd.demo.chain.of.responsibility.outer.impl;

import com.hxd.demo.chain.of.responsibility.model.TestDTO;
import com.hxd.demo.chain.of.responsibility.outer.IChainHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * 第二步处理
 *
 * @author huangxiaodong
 * @date 2021/8/25  9:48
 */
@Slf4j
@Service
public class SecondOuterChainHandler implements IChainHandler {
    @Override
    public Result handle(TestDTO testDTO) {
        log.info("第二步处理，当前实体：{}", testDTO.getName());

        return Result.builder().retCode("00").retMsg("成功").build();
    }
}
