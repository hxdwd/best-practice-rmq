package com.hxd.demo.chain.of.responsibility.outer;

import com.alibaba.fastjson.JSON;
import com.hxd.demo.chain.of.responsibility.model.TestDTO;
import com.hxd.demo.chain.of.responsibility.outer.impl.FirstOuterChainHandler;
import com.hxd.demo.chain.of.responsibility.outer.impl.SecondOuterChainHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Objects;

/**
 * 责任链处理入口
 *
 * @author huangxiaodong
 * @date 2021/8/25  9:51
 */
@Slf4j
@Service
public class TestDtoHandler {

    private final Map<String, IChainHandler> handlerMap;

    public TestDtoHandler(Map<String, IChainHandler> handlerMap) {
        this.handlerMap = handlerMap;
    }

    public void test(TestDTO testDTO) {
        for (Handlers handler : Handlers.values()) {
            // bean的名称是首字母小写
            String beanName = lowerFirstChar(handler.handler.getSimpleName());
            IChainHandler realHandler = handlerMap.get(beanName);
            if (Objects.nonNull(realHandler)) {
                IChainHandler.Result result = realHandler.handle(testDTO);
                if (!"00".equals(result.getRetCode())) {
                    // 处理失败，停止流程
                    log.warn("责任链终止，返回结果为：{}", JSON.toJSONString(result));
                    break;
                }
            } else {
                log.warn("未知处理器");
            }
        }
    }

    /**
     * 字符首字母小写
     */
    private String lowerFirstChar(String str) {
        if (null == str || str.isEmpty()) {
            return null;
        }
        char[] chars = str.toCharArray();
        chars[0] += 32;
        return String.valueOf(chars);
    }

    public enum Handlers {
        /**
         * 第一步处理
         */
        FIRST_HANDLE("第一步处理", FirstOuterChainHandler.class),
        /**
         * 第二步处理
         */
        SECOND_HANDLE("第二步处理", SecondOuterChainHandler.class),

        ;
        public final String desc;
        public final Class<? extends IChainHandler> handler;

        Handlers(String desc, Class<? extends IChainHandler> handlerClass) {
            this.desc = desc;
            this.handler = handlerClass;
        }
    }

}
