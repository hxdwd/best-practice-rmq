package com.hxd.demo.chain.of.responsibility.inner.chain;

import com.hxd.demo.chain.of.responsibility.inner.AbstractChainHandler;
import com.hxd.demo.chain.of.responsibility.model.TestDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * 2 nd step to handle chain
 *
 * @author huangxiaodong
 * @date 2021/8/24  17:51
 */
@Slf4j
@Service
public class SecondHandler extends AbstractChainHandler<TestDTO> {

    @Override
    protected void handleRequest(TestDTO msg) {
        log.info("第二步处理，当前实体：{}", msg.getName());
        log.info("责任链终止");
    }
}
