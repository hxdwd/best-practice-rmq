package com.hxd.demo.chain.of.responsibility.inner;

import java.io.Serializable;
import java.util.Optional;

/**
 * 消息处理器 责任链抽象类
 *
 * @author huangxiaodong
 * @date 2021/6/29  11:41
 */
public abstract class AbstractChainHandler<T extends Serializable> {
    /**
     * 下一个责任链节点
     */
    protected AbstractChainHandler<T> nextHandler;

    public void handle(T msg) {
        // 本次处理
        handleRequest(msg);

        // 流转到下一个节点处理
        Optional.ofNullable(nextHandler).ifPresent(handler -> handler.handle(msg));
    }

    /**
     * 本次处理器处理逻辑
     *
     * @param msg 流水/回执
     */
    protected abstract void handleRequest(T msg);
}
