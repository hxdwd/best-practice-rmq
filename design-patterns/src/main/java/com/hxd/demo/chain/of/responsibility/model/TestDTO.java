package com.hxd.demo.chain.of.responsibility.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import java.io.Serializable;

/**
 * test dto
 *
 * @author huangxiaodong
 * @date 2021/8/24  17:47
 */
@Data
@AllArgsConstructor
public class TestDTO implements Serializable {
    private String name;
    private String userId;
}
