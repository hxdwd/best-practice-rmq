package com.hxd.demo.strategy;

/**
 * 策略模式 策略算法接口
 *
 * @author huangxiaodong
 * @date 2021/10/18  10:23
 */
public interface CashSuperCalculator {
    /**
     * 计算算法
     *
     * @param originAmount 原始金额
     * @return 计算结果
     */
    double calculate(double originAmount);
}
