package com.hxd.demo.strategy.context;

import com.hxd.demo.strategy.CashSuperCalculator;

import javax.activation.MailcapCommandMap;
import java.util.Random;

/**
 * 策略上下文控制
 *
 * @author huangxiaodong
 * @date 2021/10/18  10:36
 */
public class CashSuperContext {

    private CashSuperCalculator calculator;

    public CashSuperContext(CashSuperCalculator calculator) {
        this.calculator = calculator;
    }

    public double getResult(double originAmount) {
        return calculator.calculate(originAmount);
    }

    public static void main(String[] args) {
        System.out.println(100 + new Random().nextInt(100));
    }
}
