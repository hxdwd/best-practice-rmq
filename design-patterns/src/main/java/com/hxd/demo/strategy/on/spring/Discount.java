package com.hxd.demo.strategy.on.spring;

import org.springframework.stereotype.Service;

/**
 * 打折计算器
 *
 * @author huangxiaodong
 * @date 2021/10/18  10:35
 */
@Service
public class Discount implements CashSuperCalculator {

    private double discountRate = 0.8;

    @Override
    public double calculate(double originAmount) {
        return originAmount * discountRate;
    }
}
