package com.hxd.demo.strategy.on.spring;

import org.springframework.stereotype.Service;

/**
 * 满减计算器
 *
 * @author huangxiaodong
 * @date 2021/10/18  10:29
 */
@Service
public class FullReduction implements CashSuperCalculator {

    /**
     * 满减的起始金额
     */
    private double fullAmount;
    /**
     * 满减的优惠金额
     */
    private double reduceAmount;

    @Override
    public double calculate(double originAmount) {
        // 付款金额大于满减，即减去优惠金额
        return originAmount - (originAmount > fullAmount ? reduceAmount : 0);
    }
}
