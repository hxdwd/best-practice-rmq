package com.hxd.demo.strategy.on.spring.context;


import com.hxd.demo.strategy.on.spring.CashSuperCalculator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * 策略上下文控制
 *
 * @author huangxiaodong
 * @date 2021/10/18  10:36
 */
@Component
public class CashSuperContextOnSpring {

    private Map<String, CashSuperCalculator> map;

    @Autowired
    public CashSuperContextOnSpring(Map<String, CashSuperCalculator> map) {
        this.map = map;
    }

    /**
     * 计算优惠金额
     *
     * @param originAmount         原始金额
     * @param discountStrategyType 优惠类型
     * @return /
     */
    public double getResult(double originAmount, String discountStrategyType) {
        return map.get(discountStrategyType).calculate(originAmount);
    }
}
