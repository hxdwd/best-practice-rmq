package com.hxd.demo.strategy;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * 打折计算器
 *
 * @author huangxiaodong
 * @date 2021/10/18  10:35
 */
public class Discount implements CashSuperCalculator {

    private double discountRate;

    public Discount(double discountRate) {
        this.discountRate = discountRate;
    }

    @Override
    public double calculate(double originAmount) {
        return originAmount * discountRate;
    }
}
