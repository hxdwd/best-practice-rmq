package com.hxd.demo.chain.of.responsibility.outer;

import com.alibaba.fastjson.JSON;
import com.hxd.demo.Application;
import com.hxd.demo.chain.of.responsibility.model.TestDTO;
import jdk.nashorn.internal.scripts.JO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("外部责任链测试")
@Slf4j
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = Application.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class TestDtoHandlerTest {

    @Autowired
    private TestDtoHandler handler;

    @Test
    void test1() {
        TestDTO testDTO = new TestDTO("test", "no.1");
        handler.test(testDTO);
        Assertions.assertTrue(true);
    }


    @Test
    void test() {
        Rectangular r2 = new Rectangular(1, 6, 3, 4);
        Rectangular r1 = new Rectangular(3, 4, 3, 3);
        Rectangular r3 = new Rectangular(3, 3, 1, 1);

        log.info("{}", JSON.toJSONString(getCrossRect(getCrossRect(r1, r2), getCrossRect(r2, r3))));
        log.info("{}", JSON.toJSONString(getCrossRect(r1, r2)));
        log.info("{}", JSON.toJSONString(getCrossRect(r2, r3)));

    }

    @Data
    @Builder
    @AllArgsConstructor
    public static class Rectangular {
        private int xCoordinate;
        private int yCoordinate;
        private int length;
        private int width;
    }

    private Rectangular getCrossRect(Rectangular r1, Rectangular r2) {
        if (null == r1 || null == r2) {
            return null;
        }
        // 取xy轴相交的长度
        int crossXLength = getDistanceFromTwoLine(r1.getXCoordinate(), r1.getXCoordinate() + r1.getLength(),
                r2.getXCoordinate(), r2.getXCoordinate() + r2.getLength());
        int crossYLength = getDistanceFromTwoLine(r1.getYCoordinate() - r1.getWidth(), r1.getYCoordinate(),
                r2.getYCoordinate() - r2.getWidth(), r2.getYCoordinate());
        // 取交点
        if (crossXLength > 0 && crossYLength > 0) {
            return new Rectangular(Math.max(r1.getXCoordinate(), r2.getXCoordinate()),
                    Math.min(r1.getYCoordinate(), r2.getYCoordinate()),
                    crossXLength,
                    crossYLength);
        } else {
            return null;
        }
    }

    /**
     * 获取两个线段之间的相交长度
     */
    private int getDistanceFromTwoLine(int start1, int end1, int start2, int end2) {
        // 取右边的更大数
        int countStart = Math.max(start1, start2);
        int length = 0;
        while ((countStart >= start1 && countStart <= end1) && (countStart >= start2 && countStart <= end2)) {
            countStart++;
            length++;
        }
        return length - 1;
    }

    @Test
    void testR() {
        String str = null;
        log.info("{}", str.split(","));
    }
}