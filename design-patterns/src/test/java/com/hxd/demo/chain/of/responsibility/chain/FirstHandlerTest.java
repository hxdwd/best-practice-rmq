package com.hxd.demo.chain.of.responsibility.chain;

import com.hxd.demo.Application;
import com.hxd.demo.chain.of.responsibility.inner.chain.FirstHandler;
import com.hxd.demo.chain.of.responsibility.model.TestDTO;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@DisplayName("内部责任链测试")
@Slf4j
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = Application.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class FirstHandlerTest {

    @Autowired
    private FirstHandler firstHandler;

    @Test
    void handleRequest() {
        TestDTO testDTO = new TestDTO("test","no.1");
        firstHandler.handle(testDTO);
        Assertions.assertTrue(true);
    }
}