package com.hxd.demo.strategy.context;

import com.hxd.demo.Application;
import com.hxd.demo.strategy.CashSuperCalculator;
import com.hxd.demo.strategy.FullReduction;
import com.hxd.demo.strategy.on.spring.context.CashSuperContextOnSpring;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("策略模式测试")
@Slf4j
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = Application.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class CashSuperContextTest {

    @Test
    void getResult() {
        double originAmount = 566;
        // 满减 满四百减一百
        CashSuperContext context = new CashSuperContext(new FullReduction(400, 100));
        // 结果
        log.info("满减后：{}元", context.getResult(originAmount));

        Assertions.assertTrue(true);
    }

    @Autowired
    private CashSuperContextOnSpring cashSuperContext;

    @Test
    void getResultOnSpring() {
        log.info("打折后：{}元",cashSuperContext.getResult(500, "discount"));
        Assertions.assertTrue(true);

    }
}